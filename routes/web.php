<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/country-city-division','CountryDivisionCityController@index');
Route::post('/get-division','CountryDivisionCityController@getDivision')->name('get.division');

Route::get('/file-upload','FileuploadController@index');
Route::post('store','FileuploadController@store')->name('file.store');
Route::get('show','FileuploadController@show');