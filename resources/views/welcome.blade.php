<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>COUNTRY|DIVISION|CITY</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6" style="margin-top: 20px">
            <div class="col-md-5">
                {{ Form::open(['action'=>'','method'=>'get']) }}
                <div class="form-row">
                    <div class="form-group col-md-3" >
                        {{ Form::label('country_id', 'Country', ['class'=>'']) }}
                        {{ Form::select('unit_id','',null,['class'=>'form-control','placeholder'=>'Select Country','required','id'=>'country_id'])}}
                    </div>

                    <div class=" col-md-2">
                        {{ Form::label('division_id', 'Division', ['class'=>'']) }}
                        {{ Form::select('section_id','',null,['class'=>'form-control','id'=>'division_id','placeholder'=>'Select Division'])}}
                    </div>
                    <div class=" col-md-2" >
                        {{ Form::label('city_id', 'City', ['class'=>'']) }}
                        {{ Form::select('city_id',[''],true,['class'=>'form-control','id'=>'city_id','placeholder'=>'Select City'])}}
                    </div>
                    <div class=" col-md-1" style="margin-top:26px">
                        <input type="submit" class="btn btn-info btn-sm" value="search">
                    </div>
                </div>
                {{  Form::close() }}

            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    $(document).on('change','#country_id',function () {
        var country = $(this).val();
        var token = "{{ csrf_token() }}";

        $.ajax({
            method:"post",
            url:"{{ route('get.division') }}",
            data:{country:country,"_token":token},
            dataType:"html",
            success:function (response) {
                console.log('well');
                if (response.length!=0){
                    $("#division_id").html(response);
                }else{
                    $("#division_id").html("").css({
                        'background':'#DD5246',
                        'text-align':"center"
                    });
                }
            },
            error:function (err) {
                console.log(err);
            }

        });
    });
</script>
</body>
</html>