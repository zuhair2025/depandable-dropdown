<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>COUNTRY|DIVISION|CITY</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6" style="margin-top: 20px">
                {{ Form::open() }}

                    <div class="form-group">
                        <label>Country</label>

                            {{--{{dd($country)}}--}}
                        <select class="form-control" id="country">
                            <option>Select Country</option>
                            @foreach($countries as $key => $country)
                                <option value="{{++$key}}">{{$country->name}}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="form-group">
                        <label>Division</label>
                        <select class="form-control" name="name" id="division">
                        </select>
                    </div>
                    {{--<div class="form-group" >--}}
                        {{--<label> Country</label>--}}
                        {{--<select class="form-control" id="exampleFormControlSelect1">--}}
                            {{--<option>Bangladesh</option>--}}
                            {{--<option>2</option>--}}
                            {{--<option>3</option>--}}
                            {{--<option>4</option>--}}
                            {{--<option>5</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <input type="submit" class="btn btn-info" value="SUBMIT">
                    </div>

                {{  Form::close() }}

        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
$(document).on('change','#country',function(){
var country = $(this).val();
//alert(country);

$.ajax({
    method:'post',
    url: '{{route('get.division')}}',
    data: {country:country,"_token":"{{csrf_token()}}"},
    dataType:'html',
    success:function(response){
        //alert(response);
        $("#division").html(response);
    }
});
});
</script>


</body>
</html>
