<?php

namespace App\Http\Controllers;

use App\Division;
use Illuminate\Http\Request;
use App\Country;
use PhpParser\Node\Expr\AssignOp\Div;

class CountryDivisionCityController extends Controller
{
    public function index(){
        $countries = Country::all();
        $divisions = Division::all();
        return view('country-city',compact('countries','divisions'));
    }

    public function getDivision(Request $request){
        $division = Division::where('country_id',$request->country)->get();
        echo '<option value="0">Select Division</option>';
        foreach ($division as $info){
            echo '<option value="'.$info->id.'">'.$info->name.'</option>';
        }
    }
}
