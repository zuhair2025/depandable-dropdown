<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name'];

    public function divisions(){

        return $this->hasMany(Division::class);
    }
}
